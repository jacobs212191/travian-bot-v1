# Travian Bot V1

Python with selenium simple data-scraping and automatic interfacing bot for playing travian offering basic features like farm list raiding and trading

follow instructions here https://selenium-python.readthedocs.io/ and then:
```
python travianBotV1.py

or

python3 travianBotV1.py
```