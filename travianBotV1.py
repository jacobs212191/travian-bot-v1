from selenium import webdriver
import time
import pickle
import string
from selenium.webdriver.common.by import By

from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def login(driver,un,passwd):
	driver.get("https://ts9.x1.europe.travian.com/logout")
	time.sleep(2)
	cookiesb = driver.find_element_by_id('cmpbntyestxt')
	cookiesb.click()
	time.sleep(0.2)
	elm = driver.find_element_by_name("name")
	elm.send_keys(un)
	time.sleep(0.2)
	elm = driver.find_element_by_name("password")
	elm.send_keys(passwd)
	time.sleep(0.2)
	b=driver.find_element_by_xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div/div[3]/div[1]/div[2]/form/table/tbody/tr[5]/td[2]/button")
	b.click()
	time.sleep(2)

def setUpTrade(min_id,max_id):
	driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[1]/td[1]/input').clear()
	driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[2]/td[1]/input').clear()

	sellSelect1 = Select(driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[1]/td[2]/select'))
	sellSelect2 = Select(driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[2]/td[2]/select'))

	if((min_id == 0 or min_id == 1) and (max_id == 2 or max_id == 3)):
		time.sleep(0.3)
		sellSelect1.select_by_index(max_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[1]/td[1]/input').send_keys(750)

		time.sleep(0.3)
		sellSelect2.select_by_index(min_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[2]/td[1]/input').send_keys(1250)

	if((min_id == 2 or min_id == 3) and (max_id == 0 or max_id == 1)):
		time.sleep(0.3)
		sellSelect1.select_by_index(max_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[1]/td[1]/input').send_keys(750)

		time.sleep(0.3)
		sellSelect2.select_by_index(min_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[2]/td[1]/input').send_keys(1350)

	if((min_id == 3) and (max_id == 2)):
		time.sleep(0.3)
		sellSelect1.select_by_index(max_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[1]/td[1]/input').send_keys(750)

		time.sleep(0.3)
		sellSelect2.select_by_index(min_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[2]/td[1]/input').send_keys(1250)

	if((min_id == 2) and (max_id == 3)):
		time.sleep(0.3)
		sellSelect1.select_by_index(max_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[1]/td[1]/input').send_keys(750)

		time.sleep(0.3)
		sellSelect2.select_by_index(min_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[2]/td[1]/input').send_keys(1250)

	if((min_id == 0 or min_id == 1) and (max_id == 0 or max_id == 1)):
		time.sleep(0.3)
		sellSelect1.select_by_index(max_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[1]/td[1]/input').send_keys(750)

		time.sleep(0.3)
		sellSelect2.select_by_index(min_id)
		driver.find_element_by_xpath('//*[@id="sell"]/tbody/tr[2]/td[1]/input').send_keys(1250)

	time.sleep(0.3)
	driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/form/button').click()

def sellBuyRes():
	driver.get('https://ts9.x1.europe.travian.com/build.php?t=2&id=20&gid=17&t=2')
	time.sleep(1)
	for y in range(1):
		for x in range(5):
			if(x == 1):
				res = driver.find_element_by_xpath('//*[@id="l1"]')
				wood = int(res.text.replace('.',''))
			if(x == 2):
				res = driver.find_element_by_xpath('//*[@id="l2"]')
				clay = int(res.text.replace('.',''))
			if(x == 3):
				res = driver.find_element_by_xpath('//*[@id="l3"]')
				iron = int(res.text.replace('.',''))
			if(x == 4):
				res = driver.find_element_by_xpath('//*[@id="l4"]')
				grain = int(res.text.replace('.',''))

		minn = wood
		min_idx = 0
		maxx = wood
		max_idx = 0
		idx = 0
		for r in [wood,clay,iron,grain]:
			if(r>maxx):
				maxx=r
				max_idx=idx
			if(r<minn):
				minn=r
				min_idx=idx
			idx += 1


		print(wood)
		print(clay)
		print(iron)
		print(grain)
		print(max_idx)
		print(min_idx)
			
		setUpTrade(min_idx,max_idx)
		
def buildRandomField():
	driver.find_element_by_class_name('resourceView').click()
	time.sleep(1)
	res_fields = driver.find_elements_by_xpath('//*[@id="resourceFieldContainer"]/a')
	for field in res_fields:
		print(field.get_attribute('class').split(' ')[0])
		if(field.get_attribute('class').split(' ')[0] == 'good'):
			#fields = field.get_attribute("onclick").split('.')
			#field = fields[2]+'.'+fields[3]
			print(field)
			#field = field.split("'")[1]
			phpL = field.get_attribute('href')
			field = 'https://ts9.x1.europe.travian.com/build.php?'+phpL
			driver.get(phpL)

			time.sleep(1)
			button = driver.find_element_by_xpath('//*[@class="textButtonV1 green build videoFeatureButton"]')
			# button = driver.find_element_by_xpath('//*[@class="textButtonV1 green build"]')
			button.click()
			
			time.sleep(5)
			WebDriverWait(driver, 5).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH,'//*[@id="videoArea"]')))
			WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'atg-gima-big-play-button-outer'))).click()
			time.sleep(40)
			
			break

def raid_oasis():
	
	File_object = open(r'oaze.pickle',"rb")
	oasis = pickle.load(File_object)
	File_object.close()

	for kords in oasis:
		
		printable = set(string.printable)

		xk = kords[0]
		minus=False
		if(len(xk)>4):
			minus=True
		xk = ''.join(filter(lambda x: x in printable, xk))
		if(minus):
			xk='-'+xk

		yk = kords[1]
		minus=False
		if(len(yk)>4):
			minus=True
		yk = ''.join(filter(lambda x: x in printable, yk))
		if(minus):
			yk='-'+yk

		#print("https://ts6.balkans.travian.com/position_details.php?x="+xk+"&y="+yk)
		driver.get("https://ts9.x1.europe.travian.com/position_details.php?x="+xk+"&y="+yk)
		time.sleep(1)
		
		sentTroops = False
		try:		
			animals_text = driver.find_elements_by_xpath('//*[@id="troop_info"]')[0]
			animals = animals_text.find_elements_by_xpath(".//*")[0].find_elements_by_xpath(".//*")[0].find_elements_by_xpath(".//*")
			print(animals[2].text)
			# if(animals[1].text > 1):

			# 	button = driver.find_element_by_xpath('//*[@id="tileDetails"]/div[1]/div[1]/div[2]/a')
			# 	button.click()
			# 	time.sleep(1)
			# 	inputTT = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/table/tbody/tr[1]/td[1]/input')
			# 	time.sleep(0.3)
			# 	inputTT.send_keys('3')
			# 	time.sleep(0.5)

			# 	if(inputTT.text != '2' and inputTT.text != '1' and inputTT.text != '0'):
			# 		button = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/button')
			# 		button.click()
			# 		time.sleep(1)
			# 		howMany = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/table[2]/tbody[2]/tr/td[1]')
			# 		if(howMany.text != '2' and howMany.text != '1' and howMany.text != '0'):
			# 			button = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/div[1]/button[3]')
			# 			button.click()

			if(int(animals[2].text) > 1):
				button = driver.find_element_by_xpath('//*[@id="tileDetails"]/div[1]/div[1]/div[2]/a')
				button.click()
				time.sleep(1)
				try:
					inputHero = driver.find_element_by_xpath('//*[@id="troops"]/tbody/tr[3]/td[4]/a')
					inputHero.click()
					# inputSwordman = driver.find_element_by_xpath('//*[@id="troops"]/tbody/tr[3]/td[2]/a')
					# inputSwordman.click()
					button = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/button')
					button.click()
					time.sleep(1)
					# how_many = driver.find_element_by_xpath('//*[@id="troopSendForm"]/table[2]/tbody[2]/tr/td[11]').text
					# print(how_many)
					# if(int(how_many)>0):
					button = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/div[1]/button[3]')
					button.click()
					sentTroops = True
				except Exception as e:
					print(e)
		except Exception as e:
			print(e)
		if sentTroops:
			break

def farmList():
	driver.get('https://ts9.x1.europe.travian.com/build.php?id=39&gid=16&tt=99')
	time.sleep(1)
	greenButtons = driver.find_elements_by_xpath('//*[@class="textButtonV1 green startButton"]')
	i = 0
	for gb in greenButtons:
		if gb.text == 'Start':
			gb.click()
			time.sleep(0.5)
	time.sleep(0.1)

def getBuildinLvL(elm):
	return elm.find_elements_by_xpath(".//*")[0].find_elements_by_xpath(".//*")[0].text

def buildBuilding(building):
	try:
		building.click()
		time.sleep(1)
		button = driver.find_element_by_xpath('//*[@class="textButtonV1 green build videoFeatureButton"]')
		button.click()
		
		time.sleep(1)
		WebDriverWait(driver, 5).until(EC.frame_to_be_available_and_switch_to_it((By.XPATH,'//*[@id="videoArea"]')))
		WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, 'atg-gima-big-play-button-outer'))).click()
		time.sleep(40)
	except:
		print('Already building')
	
def buildSpecialBuilding(building):
	try:
		building.click()
		time.sleep(1)
		button = driver.find_element_by_xpath('//*[@class="textButtonV1 green build"]')
		button.click()
		time.sleep(0.5)
	except:
		print('Already building')

def basicBuild():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(1)
	
	try:
		buildHouse = driver.find_element_by_xpath('//*[@class="building g15 gaul"]').find_element_by_xpath('..')
		buildHouseLvL = getBuildinLvL(buildHouse)
	except Exception as e:
		buildHouseLvL = 0
	try:
		grainery = driver.find_element_by_xpath('//*[@class="building g11 gaul"]').find_element_by_xpath('..')
		graineryLvL = getBuildinLvL(grainery)
	except Exception as e:
		graineryLvL = 0
	try:
		warehouse = driver.find_element_by_xpath('//*[@class="building g10 gaul"]').find_element_by_xpath('..')
		warehouseLvL = getBuildinLvL(warehouse)	
	except:
		warehouseLvL = 0

	if(int(warehouseLvL) < 5 and warehouseLvL != 0):
		buildBuilding(warehouse)
	if(int(graineryLvL) < 3 and graineryLvL != 0):
		buildBuilding(grainery)
	if(int(buildHouseLvL) < 8 and buildHouseLvL != 0):
		buildBuilding(buildHouse)

	if(int(warehouseLvL) < 7 and warehouseLvL != 0):
		buildBuilding(warehouse)
	if(int(graineryLvL) < 5 and graineryLvL != 0):
		buildBuilding(grainery)
	if(int(buildHouseLvL) < 11 and buildHouseLvL != 0):
		buildBuilding(buildHouse)

	if(int(warehouseLvL) < 12 and warehouseLvL != 0):
		buildBuilding(warehouse)
	if(int(graineryLvL) < 10 and graineryLvL != 0):
		buildBuilding(grainery)
	if(int(buildHouseLvL) < 15 and buildHouseLvL != 0):
		buildBuilding(buildHouse)

	if(int(warehouseLvL) < 13 and warehouseLvL != 0):
		buildBuilding(warehouse)
	if(int(graineryLvL) < 12 and graineryLvL != 0):
		buildBuilding(grainery)
	if(int(buildHouseLvL) < 20 and buildHouseLvL != 0):
		buildBuilding(buildHouse)

	if(int(warehouseLvL) < 20 and warehouseLvL != 0):
		buildBuilding(warehouse)
	if(int(graineryLvL) < 20 and graineryLvL != 0):
		buildBuilding(grainery)

def buildResHouses():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(2)

	try:
		clayHouse = driver.find_element_by_xpath('//*[@class="building g6 gaul"]').find_element_by_xpath('..')
		clayHouseLvL = getBuildinLvL(clayHouse)
	except:
		clayHouseLvL = 0
	try:
		woodHouse = driver.find_element_by_xpath('//*[@class="building g5 gaul"]').find_element_by_xpath('..')
		woodHouseLvL = getBuildinLvL(woodHouse)
	except:
		woodHouseLvL = 0
	try:
		ironHouse = driver.find_element_by_xpath('//*[@class="building g7 gaul"]').find_element_by_xpath('..')
		ironHouseLvL = getBuildinLvL(ironHouse)
	except:
		ironHouseLvL = 0
	try:
		graiMill = driver.find_element_by_xpath('//*[@class="building g8 gaul"]').find_element_by_xpath('..')
		graiMillLvL = getBuildinLvL(graiMill)
	except:
		graiMillLvL = 0
	try:
		bakery = driver.find_element_by_xpath('//*[@class="building g9 gaul"]').find_element_by_xpath('..')
		bakeryLvL = getBuildinLvL(bakery)
	except Exception as e:
		bakeryLvL =  0

	if(int(clayHouseLvL) < 5 and clayHouseLvL != 0):
		buildBuilding(clayHouse)
	if(int(woodHouseLvL) < 5 and woodHouseLvL != 0):
		buildBuilding(woodHouse)
	if(int(ironHouseLvL) < 5 and ironHouseLvL != 0):
		buildBuilding(ironHouse)
	if(int(graiMillLvL) < 5 and graiMillLvL != 0):
		buildBuilding(graiMill)
	if(int(bakeryLvL) < 5 and bakeryLvL != 0):
		buildBuilding(bakery)

def buildArmy():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(2)
	
	try:
		barracks = driver.find_element_by_xpath('//*[@class="building g19 gaul"]').find_element_by_xpath('..')
		barracksLvL = getBuildinLvL(barracks)
	except:
		barracksLvL = 0
	try:
		horseHouse = driver.find_element_by_xpath('//*[@class="building g20 gaul"]').find_element_by_xpath('..')
		horseHouseLvL = getBuildinLvL(horseHouse)
	except:
		horseHouseLvL = 0
	try:
		armorey = driver.find_element_by_xpath('//*[@class="building g13 gaul"]').find_element_by_xpath('..')
		armoreyLvL = getBuildinLvL(armorey)
	except:
		armoreyLvL = 0

	# wall = driver.find_element_by_xpath('//*[@class="building g33 gaul"]').find_element_by_xpath('..')
	# wallLvL = getBuildinLvL(wall)
	if(int(armoreyLvL) < 5 and armoreyLvL != 0):
		buildBuilding(armorey)
	if(int(barracksLvL) < 5 and barracksLvL != 0):
		buildBuilding(barracks)
	if(int(horseHouseLvL) < 5 and horseHouseLvL != 0):
		buildBuilding(horseHouse)
	if(int(armoreyLvL) < 10 and armoreyLvL != 0):
		buildBuilding(armorey)
	if(int(barracksLvL) < 10 and barracksLvL != 0):
		buildBuilding(barracks)
	if(int(horseHouseLvL) < 10 and horseHouseLvL != 0):
		buildBuilding(horseHouse)
	if(int(armoreyLvL) < 15 and armoreyLvL != 0):
		buildBuilding(armorey)
	if(int(barracksLvL) < 15 and barracksLvL != 0):
		buildBuilding(barracks)
	if(int(horseHouseLvL) < 15 and horseHouseLvL != 0):
		buildBuilding(horseHouse)
	if(int(armoreyLvL) < 20 and armoreyLvL != 0):
		buildBuilding(armorey)
	if(int(barracksLvL) < 20 and barracksLvL != 0):
		buildBuilding(barracks)
	if(int(horseHouseLvL) < 20 and horseHouseLvL != 0):
		buildBuilding(horseHouse)
	# if(int(wallLvL) < 20):
	# 	buildBuilding(wall)
	
def buildCulture():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(2)
	
	# CULTURE BUILDINGS
	try:
		marketPlace = driver.find_element_by_xpath('//*[@class="building g17 gaul"]').find_element_by_xpath('..')
		marketPlaceLvL = getBuildinLvL(marketPlace)
	except:
		marketPlaceLvL = 0
	try:
		academy = driver.find_element_by_xpath('//*[@class="building g22 gaul"]').find_element_by_xpath('..')
		academyLvL = getBuildinLvL(academy)
	except:
		academyLvL = 0
	try:
		embassy = driver.find_element_by_xpath('//*[@class="building g18 gaul"]').find_element_by_xpath('..')
		embassyLvL = getBuildinLvL(embassy)
	except:
		embassyLvL = 0
	try:
		cultureHouse = driver.find_element_by_xpath('//*[@class="building g24 gaul"]').find_element_by_xpath('..')
		cultureHouseLvL = getBuildinLvL(cultureHouse)
	except:
		cultureHouseLvL = 0

	if(int(embassyLvL)<5 and embassyLvL != 0):
		buildBuilding(embassy)
	if(int(marketPlaceLvL)<5 and marketPlaceLvL != 0):
		buildBuilding(marketPlace)
	if(int(academyLvL)<5 and academyLvL != 0):
		buildBuilding(academy)
	if(int(embassyLvL)<10 and embassyLvL != 0):
		buildBuilding(embassy)
	if(int(marketPlaceLvL)<10 and marketPlaceLvL != 0):
		buildBuilding(marketPlace)
	if(int(academyLvL)<10 and academyLvL != 0):
		buildBuilding(academy)
	if(int(embassyLvL)<15 and embassyLvL != 0):
		buildBuilding(embassy)
	if(int(marketPlaceLvL)<15 and marketPlaceLvL != 0):
		buildBuilding(marketPlace)
	if(int(academyLvL)<15 and academyLvL != 0):
		buildBuilding(academy)
	if(int(marketPlaceLvL)<20 and marketPlaceLvL != 0):
		buildBuilding(marketPlace)
	if(int(academyLvL)<20 and academyLvL != 0):
		buildBuilding(academy)
	if(int(embassyLvL)<20 and embassyLvL != 0):
		buildBuilding(embassy)
	if(int(cultureHouseLvL)<20 and cultureHouseLvL != 0):
		buildBuilding(cultureHouse)
	
def buildResidence():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(2)

	# CULTURE BUILDINGS
	try:
		palace = driver.find_element_by_xpath('//*[@class="building g26 gaul"]').find_element_by_xpath('..')
		palaceLvL = getBuildinLvL(palace)
	except:
		palaceLvL = 0

	try:
		residence = driver.find_element_by_xpath('//*[@class="building g25 gaul"]').find_element_by_xpath('..')
		residenceLvL = getBuildinLvL(residence)
	except:
		residenceLvL = 0

	try:
		academy = driver.find_element_by_xpath('//*[@class="building g22 gaul"]').find_element_by_xpath('..')
		academyLvL = getBuildinLvL(academy)
	except:
		academyLvL = 0

	if(int(palaceLvL)<10 and palaceLvL != 0):
		buildSpecialBuilding(palace)

	if(int(palaceLvL)<20 and academyLvL == 20):
		buildSpecialBuilding(palace)

	if(int(residenceLvL)<10 and residenceLvL != 0):
		buildSpecialBuilding(residence)

	if(int(residenceLvL)<20 and academyLvL == 20):
		buildSpecialBuilding(residence)

def buildCrannies():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(2)

	# CULTURE BUILDINGS
	try:
		buildWall = driver.find_element_by_xpath('//*[@class="g33Bottom gaul"]').find_element_by_xpath('..')
		wallLVL = getBuildinLvL(buildWall)				
	except:
		wallLVL = 0

	if(wallLVL < 20):
		buildBuilding(buildWall)

def testSel():

	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(1)
	build = driver.find_element_by_xpath('//*[@class="building g15 gaul"]').find_element_by_xpath('..')
	print(getBuildinLvL(build))

def chiefVillage(xk,yk):
	troops = driver.find_elements_by_xpath('//*[@class="un"]')
	for troopField in troops:
		print(troopField.text)
		if(troopField.text == 'Poglavar'):
			driver.get("https://ts9.x1.europe.travian.com/position_details.php?x="+xk+"&y="+yk)
			time.sleep(1)
			button = driver.find_element_by_xpath('//*[@id="tileDetails"]/div[1]/div[1]/div[2]/a')
			button.click()
			time.sleep(1)
			inputChief = driver.find_element_by_xpath('//*[@id="troops"]/tbody/tr[1]/td[4]/input')
			inputChief.send_keys('1')
			inputSwordman = driver.find_element_by_xpath('//*[@id="troops"]/tbody/tr[2]/td[1]/input')
			inputSwordman.send_keys('50')
			button = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/button')
			button.click()
			time.sleep(1)
			button = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/div[1]/button[3]')
			button.click()

def throwFest():
	try:
		driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
		time.sleep(1)
		cultureHouse = driver.find_element_by_xpath('//*[@class="building g24 gaul"]').find_element_by_xpath('..').click()
		time.sleep(1)
		festButton = driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div/div[4]/div/div/div[3]/button')
		festButton.click()
		time.sleep(0.3)
		driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
		time.sleep(0.3)
	except:
		driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
		time.sleep(0.3)
		print('No fest house')

def send750grain(villaNumber,villa):
	driver.find_elements_by_xpath('//*[@class="dropContainer"]')[villaNumber].click()
	time.sleep(1)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.2)
	Select(driver.find_element_by_xpath('//*[@id="x2"]')).select_by_index(1)
	time.sleep(0.2)
	driver.find_element_by_xpath('//*[@id="xCoordInput"]').send_keys(villa[0])
	time.sleep(0.2)
	driver.find_element_by_xpath('//*[@id="yCoordInput"]').send_keys(villa[1])
	time.sleep(0.2)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.4)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.4)

def send1500grain(villaNumber,villa):
	driver.find_elements_by_xpath('//*[@class="dropContainer"]')[villaNumber].click()
	time.sleep(1)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.5)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.5)
	Select(driver.find_element_by_xpath('//*[@id="x2"]')).select_by_index(1)
	time.sleep(0.4)
	driver.find_element_by_xpath('//*[@id="xCoordInput"]').send_keys(villa[0])
	time.sleep(0.4)
	driver.find_element_by_xpath('//*[@id="yCoordInput"]').send_keys(villa[1])
	time.sleep(0.4)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.5)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.5)

def send3000grain(villaNumber,villa):
	driver.find_elements_by_xpath('//*[@class="dropContainer"]')[villaNumber].click()
	time.sleep(1)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="xCoordInput"]').send_keys(villa[0])
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="yCoordInput"]').send_keys(villa[1])
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.5)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.5)

def send3000grain(villaNumber,villa):
	driver.find_elements_by_xpath('//*[@class="dropContainer"]')[villaNumber].click()
	time.sleep(1)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="addRessourcesLink4"]').click()
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="xCoordInput"]').send_keys(villa[0])
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="yCoordInput"]').send_keys(villa[1])
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.5)
	driver.find_element_by_xpath('//*[@id="enabledButton"]').click()
	time.sleep(0.5)

def sendGrain(villas,driver):
	driver = driver
	for villa in villas:
		driver.get('https://ts9.x1.europe.travian.com/build.php?id=20&gid=17&t=5')
		time.sleep(0.5)
		send750grain(1,villa)
		send1500grain(2,villa)
		send1500grain(4,villa)

def buildTrappers():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(2)

	trappers = driver.find_elements_by_xpath('//*[@class="building g36 gaul"]')
	for trapper in trappers:
		try:
			trapper = trapper.find_element_by_xpath('..')
			trapperLvl = getBuildinLvL(trapper)
		except:
			trapperLvl = 0

		if(int(trapperLvl)<20):
			buildBuilding(trapper)

# def sendPhalanx(xk,yk):
# 	driver.get('https://ts9.x1.europe.travian.com/build.php?newdid=19299&tt=2&targetMapId=92361&eventType=5&gid=16')
# 	time.sleep(1)
# 	driver.find_element_by_xpath('//*[@id="xCoordInput"]').send_keys(xk)
# 	driver.find_element_by_xpath('//*[@id="yCoordInput"]').send_keys(yk)
# 	time.sleep(0.5)
# 	phalanxs = driver.find_element_by_xpath('//*[@id="troops"]/tbody/tr[1]/td[1]/a')
# 	print(phalanxs.text)
# 	if(int(phalanxs.text) > 50):
# 		phalanxs.click()
# 		time.sleep(0.5)
# 		driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/button').click()
# 		time.sleep(1)
# 		driver.find_element_by_xpath('/html/body/div[3]/div[3]/div[2]/div[2]/div/div[3]/div/form/div[1]/button[3]').click()
# 		time.sleep(1)

def buildHeromason():
	driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
	time.sleep(2)

	try:
		buildHeromason = driver.find_element_by_xpath('//*[@class="building g37 gaul"]').find_element_by_xpath('..')
		buildHeromasonLvl = getBuildinLvL(buildStonemason)
	except:
		buildHeromasonLvl = 0

	if(int(buildHeromasonLvl)<15):
		buildBuilding(buildHeromason)



def buildDeff():
	driver.get('https://ts9.x1.europe.travian.com/dorf1.php?newdid=19299&')
	time.sleep(1)
	driver.get('https://ts9.x1.europe.travian.com/build.php?gid=19')
	time.sleep(1)
	driver.find_element_by_xpath('//*[@id="nonFavouriteTroops"]/div[1]/div/div[2]/div[4]/input').send_keys(40)
	time.sleep(0.3)
	driver.find_element_by_xpath('//*[@class="textButtonV1 green startTraining"]').click()
	time.sleep(0.7)

def auctionCages():
	driver.get('https://ts9.x1.europe.travian.com/hero/auction?tab=buy')
	time.sleep(0.5)
	driver.find_element_by_xpath('//*[@class="itemCategory itemCategory_cage"]').click()
	time.sleep(1)
	i=0
	ponudiButtons = driver.find_elements_by_xpath('//*[@class="bidButton openedClosedSwitch switchClosed"]')
	for x in range(5):
		ponudiButtons[x].click()
		time.sleep(1)
		driver.find_element_by_xpath('//*[@class="maxBid text"]').send_keys(103)
		time.sleep(0.3)
		driver.find_element_by_xpath('//button[@value="Ponudi"]').click()
		time.sleep(1)
		ponudiButtons = driver.find_elements_by_xpath('//*[@class="bidButton openedClosedSwitch switchClosed"]')

index = 0
while True:
	
	for u in users:
		driver = webdriver.Firefox()
		try:
			login(driver,username,passwd)
		except Exception as e:
			print (e)
		
		# try:
		# 	villas = driver.find_elements_by_xpath('//*[@class="dropContainer"]')
		# 	i = 0
		# 	for v in villas:
		# 		if(i != 0):
		# 			driver.find_elements_by_xpath('//*[@class="dropContainer"]')[i].click()
		# 			time.sleep(1)
		# 			sellBuyRes()
		# 		i = i + 1
		# except Exception as e:
		# 	print(e)

		# try:
		# 	driver.find_elements_by_xpath('//*[@class="dropContainer"]')[0].click()
		# 	time.sleep(1)
		# 	chiefVillage('-73','-32')
		# except Exception as e:
		# 	print(e)
		
		if(u == 'madgaul'):
			try:
				driver.find_elements_by_xpath('//*[@class="dropContainer"]')[0].click()
				farmList()
			except Exception as e:
				print(e)
			try:
				driver.find_elements_by_xpath('//*[@class="dropContainer"]')[0].click()
				time.sleep(1)
				raid_oasis()
			except Exception as e:
				print(e)
			# try:
			# 	buildDeff()
			# except Exception as e:
			# 	print (e)
	
		try:
			villas = driver.find_elements_by_xpath('//*[@class="dropContainer"]')
			i = 0
			for v in villas:
				driver.find_elements_by_xpath('//*[@class="dropContainer"]')[i].click()
				time.sleep(1)
				i = i + 1
				try:
					buildHeromason()
				except Exception as e:
					print(e)
				try:
					basicBuild()
				except Exception as e:
					print(e)
				try:
					buildResHouses()
				except Exception as e:
					print(e)
				try:
					buildCrannies()
				except Exception as e:
					print(e)
				try:
					buildTrappers()
				except Exception as e:
					print(e)
				try:
					buildResidence()
				except Exception as e:
					print(e)
				try:
					buildRandomField()
				except Exception as e:
					print(e)
				try:
					buildCulture()
				except Exception as e:
					print(e)
				try:
					buildArmy()
				except Exception as e:
					print(e)

				try:
					throwFest()
					driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
					time.sleep(0.3)
				except Exception as e:
					driver.get('https://ts9.x1.europe.travian.com/dorf2.php')
					print(e)
					time.sleep(0.3)
		except Exception as e:
			print (e)

		

		# try:
		# 	villas = driver.find_elements_by_xpath('//*[@class="dropContainer"]')
		# 	i = 0
		# 	for v in villas:
				
		# 		driver.find_elements_by_xpath('//*[@class="dropContainer"]')[i].click()
		# 		time.sleep(1)AD5
		
		# 		troops = driver.find_elements_by_xpath('//*[@class="un"]')
		# 		for troopField in troops:
		# 			print(troopField.text)
		# 			if(troopField.text == 'Falang'):
		# 		i = i + 1
		# 		sendPhalanx('-70','-30')
		# except Exception as e:
		# 	print(e)

		# try:
		# 	sendGrain([['-79','-65']],driver)
		# except Exception as e:
		# 	print(e)

		# try:
		# 	auctionCages()
		# except Exception as e:
		# 	print(e)


		driver.close() 
		time.sleep(3)

	time.sleep(5500)