from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import pickle
import os.path
import random as rand
import string


def login(driver):
	driver.get("https://ts6.x1.europe.travian.com/logout")
	time.sleep(2)
	try:
		cookiesb = driver.find_element_by_id('cmpbntyestxt')
		cookiesb.click()
	except Exception as e:
		print('nothing')
	time.sleep(0.21)
	
	elm = driver.find_element_by_name("name")
	elm.send_keys('username')
	time.sleep(0.21)
	
	elm = driver.find_element_by_name("password")
	elm.send_keys('password')
	time.sleep(0.21)
	
	b=driver.find_element_by_xpath("/html/body/div[3]/div[2]/div[2]/div[2]/div/div[3]/div[1]/div[2]/form/table/tbody/tr[5]/td[2]/button")
	b.click()
	
	time.sleep(3)


def get_oases_and_natars(margin,driver):

	counter = 0

	
	org_x = 75
	org_y = -74
	
	oases = []
	natars = []
	players = []

	for margin_x in range(-margin,margin+1):
		for margin_y in range(-margin,margin+1):
			if(counter % 10 == 0 and counter != 0):
				driver = webdriver.Firefox()
				login(driver)
				time.sleep(3)
				
			try:
				x = org_x+margin_x
				y = org_y+margin_y
				driver.get("https://ts6.x1.europe.travian.com/position_details.php?x="+str(x)+"&y="+str(y))
				time.sleep(0.7)

				again = True
				counter = 0
				while(again):
					try:
						# /html/body/div[2]/div[4]/div[1]/div[2]/div[3]/div[1]/div[1]/h1
						# /html/body/div[3]/div[4]/div[1]/div[2]/div[3]/div[1]/div[1]/h1
						# if(counter == 0):
						# 	title = driver.find_element_by_xpath('/html/body/div[3]/div[4]/div[1]/div[2]/div[3]/div[1]/div[1]/h1')
						# 	again = False
						# else:
						# 	again = False
						# 	title = driver.find_element_by_xpath('/html/body/div[2]/div[4]/div[1]/div[2]/div[3]/div[1]/div[1]/h1')
						again = False
						title = driver.find_element_by_class_name('titleInHeader')
						print(title.text)	
					except:
						counter += 1

				name_o = title.text.split(' ')[0]+' '+title.text.split(' ')[1]
				if(name_o == 'Nezasedena oaza'):
					kords = title.text.split(' ')[2]
					x = kords.split('|')[0][2:]
					y = kords.split('|')[1][:-2]
					
					print(x+','+y)
					oases.append([x,y])
					print(title.text)


				name_n = title.text.split(' ')[0]
				if(name_n == 'Natars'):
					kords = title.text.split(' ')[2]
					x = kords.split('|')[0][2:]
					y = kords.split('|')[1][:-2]
					
					#print(x+','+y)
					natars.append([x,y])
					print(title.text)
				
				File_object = open('oaze.pickle', 'wb')
				pickle.dump(oases, File_object,-1)
				File_object.close()
				
				File_object = open('natars.pickle', 'wb')
				pickle.dump(natars, File_object,-1)
				File_object.close()
				
				# File_object = open('players.pickle', 'wb')
				# pickle.dump(players, File_object,-1)
				# File_object.close()
			except:
				pass
			counter += 1
			if(counter % 10 == 0):
				driver.close()

	
	for oase in oases:
		print(oase)			
	File_object = open('oaze.pickle', 'wb')
	pickle.dump(oases, File_object,-1)
	File_object.close()

	for natar in natars:
		print(natar)
	File_object = open('natars.pickle', 'wb')
	pickle.dump(natars, File_object,-1)
	File_object.close()

	# for player in players:
	# 	print(player)
	# File_object = open('players.pickle', 'wb')
	# pickle.dump(players, File_object,-1)
	# File_object.close()

	
driver = webdriver.Firefox()

keep_trying=True
while(keep_trying):
	try:
		login(driver)
		keep_trying=False
	except:
		pass

time.sleep(2)
# try:
# 	reject_all_cookies = driver.find_element_by_xpath('//*[@id="cmpbntnotxt"]')
# 	reject_all_cookies.click()
# 	time.sleep(1)

# except:
# 	pass

get_oases_and_natars(7,driver)

driver.close()